const each = require('./each.cjs')

function flatten(elements, depth=1){
    let ans=[]
    each(elements, (item, key) => {
        if(Array.isArray(item) && depth > 0){
            ans = ans.concat(flatten(item, depth-1))
        }
        else{
            if(item != undefined)
                ans.push(item)
        }
    })
    return ans
}

module.exports = flatten