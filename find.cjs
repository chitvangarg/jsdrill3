function find(arr, cb){
    for(let item=0; item < arr.length; item++){
        if(cb(arr[item], item))
            return arr[item]
    }
    return undefined;
}

module.exports = find