function filter(arr, cb){

    const res = []

    for(let item=0; item < arr.length; item++){
        if(cb(arr[item], item, arr) === true){
            res.push(arr[item])
        }
    }
    return res;
}

module.exports = filter