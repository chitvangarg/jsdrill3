function map(arr, cb){
    const newData = [];

    for(let item = 0; item < arr.length; item++){
        newData.push(cb(arr[item], item, arr))
    }

    return newData
}

module.exports = map;