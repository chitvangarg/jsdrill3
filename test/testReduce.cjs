const data = require('../items.cjs')
const reduce = require('../reduce.cjs')

function callback(accumulator, currValue, currIdx, arr){
    accumulator = accumulator + (currValue*currIdx);

    return accumulator;
}

console.log(reduce(data, callback))