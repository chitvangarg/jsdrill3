function reduce(arr, cb, initialValue = null){

    let currIdx = 1;
    let accumulator = arr[0];

    if(initialValue != null){
        currIdx = 0;
        accumulator = initialValue;
    }

    for(let item = currIdx; item < arr.length; item++){
        accumulator = cb(accumulator, arr[item], item, arr); 
    }

    return accumulator
}

module.exports = reduce
