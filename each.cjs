function each(arr, cb){
    for(let elem = 0; elem < arr.length; elem++){
        cb(arr[elem], elem)
    }
}

module.exports = each
